'use strict';

angular.module('prototipoApp')
	.controller('ContactCtrl', ['$scope','$routeParams','$anchorScroll' ,function ($scope,$routeParams,$anchorScroll) {
			$scope.contact = window.contacts.filter(function(contact){ return contact.id===$routeParams.id; })[0];
			$scope.orders = window.salesOrders.filter(function(order){
				return order.contact.id===$scope.contact.id;
			});
			$scope.showOrders = $scope.showStatement = true;
	
			$scope.anySelected = function(){
				return $scope.orders.filter(function(order){ return order.selected; }).length > 0;
			};
			$scope.selectAll = false;
			$scope.$watch('selectAll',function(newValue){
				$scope.orders.forEach(function(order){
					order.selected = newValue;
				});
			});
			$anchorScroll();
	
		}]);
