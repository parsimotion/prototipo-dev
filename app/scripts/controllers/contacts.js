'use strict';

angular.module('prototipoApp')
	.controller('ContactsCtrl', ['$scope', function ($scope) {
			$scope.contacts = window.contacts;
			$scope.anySelected = function(){
				return $scope.contacts.filter(function(contact){ return contact.selected; }).length > 0;
			};
	
			$scope.selectAll = false;
			$scope.$watch('selectAll',function(newValue){
				$scope.contacts.forEach(function(contact){
					contact.selected = newValue;
				});
			});
		}]);
