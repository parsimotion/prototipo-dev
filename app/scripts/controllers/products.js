'use strict';

angular.module('prototipoApp')
	.controller('PriceListCtrl', ['$scope','$modalInstance','products', function ($scope,$modalInstance,products) {
	var height = 200;
	$scope.products = products;
	$scope.getHeight = function(){
		return height;
	};
	$scope.beforeRender = function(){
		var MAX_ROWS = 15;
		if(this.countRows()>MAX_ROWS){
			height =  27+23*MAX_ROWS;
		}
		else {
			height = Infinity;
		}
	};
	$scope.ok = function () {
		$modalInstance.close();
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
}]);

angular.module('prototipoApp')
	.controller('EditStocksCtrl', ['$scope','$modalInstance','products','warehouse', function ($scope,$modalInstance,products,warehouse) {
	var height = 200;
	$scope.products = products;
	$scope.warehouses = window.warehouses;
	$scope.warehouse = warehouse;
	$scope.recalcStocks = function(){
		$scope.products.forEach(function(product){
			product.stock = product[$scope.warehouse.id];
			product.newStock = product[$scope.warehouse.id];
		});
	};
	$scope.$watch('warehouse',function(){
		$scope.recalcStocks();
	},true);
	$scope.getHeight = function(){
		return height;
	};
	$scope.beforeRender = function(){
		var MAX_ROWS = 15;
		if(this.countRows()>MAX_ROWS){
			height =  27+23*MAX_ROWS;
		}
		else {
			height = Infinity;
		}
	};
	$scope.ok = function () {
		$scope.products.forEach(function(product){
			product[$scope.warehouse.id] = product.newStock;
		});
		$modalInstance.close();
	};

	$scope.cancel = function () {
		$modalInstance.dismiss('cancel');
	};
}]);

angular.module('prototipoApp')
	.controller('ProductsCtrl', ['$scope','$modal', function ($scope,$modal) {
			$scope.products = window.products;
			$scope.warehouses = window.warehouses;
			$scope.warehouse = window.warehouses[0];
			$scope.productStatusFilter = 'active';
			$scope.products.forEach(function(product){
				product.getStock = function(warehouseId){
					var stock = 0;
					product.variants.forEach(function(variant){
						window.warehouses.filter(function(w){ return warehouseId ? w.id===warehouseId : true;}).forEach(function(warehouse){
							stock += variant[warehouse.id];
						});
					});
					return stock;
				};
			});
			$scope.anySelected = function(){
				return $scope.selectedProducts().length > 0;
			};
			$scope.selectedProducts = function(){
				return $scope.products.filter(function(product){ return product.selected; });
			};
			$scope.selectAll = false;
			$scope.$watch('selectAll',function(newValue){
				$scope.products.forEach(function(product){
					product.selected = newValue;
				});
			});

			var height = 0;
			$scope.getHeight = function(){
				return height;
			};
			$scope.beforeRender = function(){
				var MAX_ROWS = 15;
				if(this.countRows()>MAX_ROWS){
					height =  27+23*MAX_ROWS;
				}
				else {
					height = Infinity;
				}
			};
			$scope.editPricelists = function(){
				$modal.open({
					templateUrl: 'views/pricelists-edit.html',
					controller: 'PriceListCtrl',
					resolve: {
						products: function () {
							return $scope.selectedProducts();
						}
					},
					windowClass: 'pricelists-modal'
				});
			};
			$scope.editStocks = function(){
				$modal.open({
					templateUrl: 'views/stocks-edit.html',
					controller: 'EditStocksCtrl',
					resolve: {
						products: function () {
							var variants = [];
							$scope.selectedProducts().forEach(function(product){
								return product.variants.forEach(function(variant){
									variant.description = product.description+' '+variant.color+' '+variant.size;
									variants.push(variant);
								});
							});
							return variants;
						},
						warehouse: function(){
							return $scope.warehouse;
						}
					},
					windowClass: 'stocks-modal'
				});
			};
		}]);
