'use strict';

Array.prototype.flatten = function() {
	var flattenedArray;

	if (this.length === 0) {
		return this;
	}
	flattenedArray = [];
	this.forEach(function(element) {
		return flattenedArray = flattenedArray.concat(element);
	});
	return flattenedArray;
};

angular.module('prototipoApp')
	.controller('OrderCtrl', ['$scope','$routeParams','$location', function ($scope,$routeParams,$location) {
			$scope.warehouses = window.warehouses;
			$scope.contacts = window.contacts;
			$scope.products = window.products;
			$scope.variants = window.products.map(function(product){ return product.variants.map(function(variant){ variant.product = { id: product.id, description: product.description }; return variant; }); }).flatten();
			var arr = $location.url().split('/');
			var action = arr[arr.length-1];
			if(action.substring(0,3)==='new'){
				var today = new Date().toISOString();
				$scope.editingOrder = {
					id: window.salesOrders[window.salesOrders.length-1].id + 1,
					lines: [],
					contact: {name: 'Consumidor Final'},
					date: today,
					warehouse: window.warehouses[0],
					shippingMethod: 'Retira',
					paymentMethod: 'Contra Entrega',
					payments: [],
					shipments: []
				};
				$scope.newOrder = true;
				if($routeParams.contactId){
					$scope.editingOrder.contact = $scope.contacts.filter(function(contact){ return contact.id===$routeParams.contactId; })[0];
				}
				$scope.$watch(function(){return $scope.editingOrder.shippingMethod;},function(newValue){
					if(newValue==='Retira'){
						$scope.editingOrder.markAsShipped=true;
						$scope.editingOrder.markAsPayed=true;
					}
					else {
						$scope.editingOrder.markAsShipped=false;
						$scope.editingOrder.markAsPayed=false;
					}
				});
			}
			else{
				$scope.order = window.salesOrders.filter(function(order){ return order.id===$routeParams.id; })[0];
				$scope.editingOrder = angular.copy($scope.order);
				if($scope.editingOrder.warehouse){
					$scope.editingOrder.warehouse = $scope.warehouses.filter(function(warehouse){ return warehouse.id===$scope.editingOrder.warehouse.id; })[0];
				}
			}

			var addDirtyLine = function(){
				$scope.editingOrder.lines.forEach(function(line){
					line.dirty = false;
				});
				var dirtyLine = {dirty:true,quantity:1};
				$scope.editingOrder.lines.push(dirtyLine);
				var unregister = $scope.$watch(function(){ return dirtyLine.product; }, function(newValue){
					if(newValue){
						addDirtyLine();
						unregister();
					}
				});
				$scope.$watch(function(){ return dirtyLine.product; }, function(newValue){
					if(newValue){
						dirtyLine.price = dirtyLine.product.price1;
					}
				});
				$scope.$watch(function(){ return dirtyLine.variant; }, function(newValue){
					if(newValue && newValue.product){
						dirtyLine.product = $scope.products.filter(function(product){ return product.id===newValue.product.id; })[0];
						dirtyLine.variant_id = newValue.id;
					}
					else {
						dirtyLine.product = undefined;
					}
				});

			};
			if(action.substring(0,4)==='edit' || action.substring(0,3)==='new'){
				addDirtyLine();
			}
			$scope.editPayment = function(payment){
				payment.previousValue = angular.copy(payment);
				payment.dirty = true;
			};
			$scope.cancelPayment = function(payment){
				if(payment.previousValue){
					var index = $scope.order.payments.indexOf(payment);
					payment.previousValue.dirty = false;
					$scope.order.payments[index] = payment.previousValue;
				}
				else{
					$scope.removePayment(payment);
				}
			};
			$scope.getShippedQuantityFor = function(product){
				var quantity = 0;
				$scope.order.shipments.forEach(function(shipment){
					shipment.products.forEach(function(line){
						if(line.product.id===product.id){
							quantity += line.quantity;
						}
					});
				});
				return quantity;
			};
			$scope.totalShipped = function(){
				var quantity = 0;
				$scope.order.lines.forEach(function(line){
					quantity += $scope.getShippedQuantityFor(line.product);
				});
				return quantity;
			};
			$scope.totalQuantity = function(){
				var quantity = 0;
				$scope.order.lines.forEach(function(line){
					quantity += line.quantity;
				});
				return quantity;
			};
			$scope.totalAmount = function(){
				var amount = 0;
				$scope.order.lines.forEach(function(line){
					amount += line.quantity * line.price;
				});
				return amount;
			};
			$scope.totalPayed = function(){
				var amount = 0;
				$scope.order.payments.forEach(function(payment){
					amount += payment.amount;
				});
				return amount;
			};

			$scope.addPayment = function() {
				var today = new Date().toISOString();
				var amount = $scope.totalAmount() - $scope.totalPayed();
				$scope.order.payments.push({dirty: true, date: today, amount: amount, method: 'Efectivo' });
			};
			$scope.removePayment = function(payment){
				if(payment.dirty && !payment.previousValue || window.confirm('Esta seguro que desea eliminar el pago?')){
					var index = $scope.order.payments.indexOf(payment);
					$scope.order.payments.splice(index, 1);
				}
			};
			$scope.paymentStatus = function(){
				if($scope.totalPayed()===0){
					return 'Pending';
				}
				else if($scope.totalPayed()<$scope.totalAmount()){
					return 'Partial';
				}
				return 'Done';
			};
			$scope.addShipment = function(){
				var calcProducts = $scope.order.lines.filter(function(line){ return line.quantity - $scope.getShippedQuantityFor(line.product) > 0; }).map(function(line){ return {product: line.product, quantity: line.quantity - $scope.getShippedQuantityFor(line.product)}; });
				$scope.order.shipments.push({dirty:true, shippingMethod: ($scope.order.shippingMethod==='A Confirmar' ? 'Retira' : 'Envio') , products: calcProducts});
			};
			$scope.removeShipment = function(shipment){
				if(shipment.dirty && !shipment.previousValue || window.confirm('Esta seguro que desea eliminar la entrega?')){
					var index = $scope.order.shipments.indexOf(shipment);
					$scope.order.shipments.splice(index, 1);
				}
			};
			$scope.editShipment = function(shipment){
				shipment.previousValue = angular.copy(shipment);
				shipment.dirty = true;
			};
			$scope.cancelShipment = function(shipment){
				if(shipment.previousValue){
					var index = $scope.order.shipments.indexOf(shipment);
					shipment.previousValue.dirty = false;
					$scope.order.shipments[index] = shipment.previousValue;
				}
				else{
					$scope.removeShipment(shipment);
				}
			};
			$scope.shippingStatus = function(){
				if($scope.totalShipped()===0){
					return 'Pending';
				}
				else if($scope.totalShipped()<$scope.totalQuantity()){
					return 'Partial';
				}
				return 'Done';
			};
			$scope.save = function(){
				$scope.editingOrder.lines.filter(function(line){ return line.dirty; }).forEach(function(line){
					var index = $scope.editingOrder.lines.indexOf(line);
					$scope.editingOrder.lines.splice(index, 1);
				});
				if($scope.newOrder){
					window.salesOrders.push($scope.editingOrder);
				}
				$location.path('/sales/'+$scope.editingOrder.id);
			};

		}]);
