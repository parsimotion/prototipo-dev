'use strict';

angular.module('prototipoApp')
	.controller('ProductCtrl', ['$scope','$routeParams','$location','$modal', function ($scope,$routeParams,$location,$modal) {
			var arr = $location.url().split('/');
			var action = arr[arr.length-1];
			if(action.substring(0,3)==='new'){
				var nextId = window.products[window.products.length-1].id+1;
				$scope.product = {
					isNew: true,
					id: nextId,
					colors: [],
					sizes: [],
					variants: []
				};
			}
			else {
				$scope.product = window.products.filter(function(product){ return product.id===$routeParams.id; })[0];
			}
			$scope.warehouses = window.warehouses;
			$scope.selectedWarehouse = $scope.warehouses[0];
			$scope.listing = window.listing;
			$scope.listing.category = window.category;
			$scope.getVariantFor = function(color,size){
				return $scope.product.variants.filter(function(variant){ return variant.color===color && variant.size===size; })[0];
			};

			$scope.stockTableWidth = function(){
				var colorsWidth = 128 * ($scope.product.colors.length + 1);
				return colorsWidth < 290 ? 290 : colorsWidth;
			};
			
			$scope.brands = ['VAM','F21'];
			$scope.categories = ['Calzado','Accesorios'];

			var setEdit = function(){
				$scope.editListing = $routeParams.editListing;
				$scope.editMeliIntegration = $routeParams.editMeliIntegration;
			};
			setEdit();
			$scope.$on('$locationChangeSuccess', function() {
				setEdit();
			});
			$scope.save = function(){
				if($scope.product.isNew){
					$scope.product.isNew = false;
					window.products.push($scope.product);
				}
				$location.path('/products/'+$scope.product.id);
			};
			$scope.cancel = function(){
				if($scope.product.isNew){
					$location.path('/products');
				}
				else{
					$location.path('/products/'+$scope.product.id);
				}
			};
			$scope.editStocks = function(){
				$modal.open({
					templateUrl: 'views/stocks-edit.html',
					controller: 'EditStocksCtrl',
					resolve: {
						products: function () {
							var variants = [];
							$scope.product.variants.forEach(function(variant){
								variant.description = $scope.product.description+' '+variant.color+' '+variant.size;
								variants.push(variant);
							});
							return variants;
						},
						warehouse: function(){
							return $scope.selectedWarehouse;
						}
					},
					windowClass: 'stocks-modal'
				});
			};
		}]);
