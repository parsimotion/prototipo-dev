'use strict';

angular.module('prototipoApp')
	.controller('SalesCtrl', ['$scope','$filter','$routeParams','$modal', function ($scope,$filter,$routeParams,$modal) {
			$scope.textSearch = '';
			$scope.filteredOrders = [];
			$scope.orders = window.salesOrders;
			$scope.warehouses = [];
			var todos = {name: 'Todos'};
			window.warehouses.forEach(function(warehouse){$scope.warehouses.push(warehouse);});
			$scope.warehouses.unshift(todos);
			$scope.warehouseFilter = todos;
			$scope.paymentMethods = ['A Confirmar', 'MercadoPago', 'Transferencia', 'Contra Entrega'];
			$scope.shippingMethods = ['A Confirmar', 'Envio', 'Retira'];

			var selectedOrders = function(){
				return $scope.orders.filter(function(order){ return order.selected; });
			};
			$scope.anySelected = function(){
				return selectedOrders().length > 0;
			};
			$scope.recalc = function(){
				var toBeConfirmed = $scope.orders.filter(function(order){return order.shippingMethod==='A Confirmar' || order.paymentMethod==='A Confirmar'; });
				var toBeShipped = $scope.orders.filter(function(order){return order.shippingMethod==='Envio' && order.shippingStatus==='Pending' && (order.paymentStatus==='Done' || order.paymentMethod==='Contra Entrega') ;});
				var waitingPayment = $scope.orders.filter(function(order){return order.shippingMethod==='Envio' && order.paymentStatus==='Pending' && order.paymentMethod!=='Contra Entrega' ;});
				var toBePickedUp = $scope.orders.filter(function(order){return order.shippingStatus==='Pending' && order.shippingMethod==='Retira' ;});
				var archived = $scope.orders.filter(function(order){return order.shippingStatus==='Done';});
				var filterOrders = function(orders){
					return $filter('filter')(orders,function(order){ return $scope.warehouseFilter.id ? order.warehouse.id===$scope.warehouseFilter.id : true; });
				};
				$scope.toBeConfirmed = filterOrders(toBeConfirmed);
				$scope.toBeShipped = filterOrders(toBeShipped);
				$scope.waitingPayment = filterOrders(waitingPayment);
				$scope.toBePickedUp = filterOrders(toBePickedUp);
				$scope.archived = filterOrders(archived);
			};
			$scope.toBeConfirmedVisible = true;
			$scope.toBeShippedVisible = true;
			$scope.waitingPaymentVisible = true;
			$scope.toBePickedUpVisible = true;
			$scope.archivedVisible = true;

			$scope.selectAll = false;
			$scope.$watch('selectAll',function(newValue){
				$scope.orders.forEach(function(order){
					order.selected = newValue;
				});
			});
			$scope.recalc();

			$scope.filter = function(value){
				if(value){
					$scope.filteredOrders = $filter('filter')($scope.orders,value);
				}
				else {
					$scope.filteredOrders = [];
				}
			};
			$scope.$watch('textSearch',$scope.filter);
			$scope.$watch('warehouseFilter',$scope.recalc);
			var setActiveTab = function(){
				$scope.activeTab = $routeParams.tab || 'toBeConfirmed';
			};
			setActiveTab();
			$scope.$on('$locationChangeSuccess', function() {
				$scope.selectAll = false;
				$scope.orders.forEach(function(order){
					order.selected = false;
				});
				setActiveTab();
			});
			$scope.deleteButtonEnabled = function(){
				var orderIsStarted = function(order){
					return order.shipments.length > 0 || order.payments.length > 0;
				};
				if(!$scope.anySelected() || selectedOrders().filter(orderIsStarted).length > 0){
					return false;
				}
				return true;
			};
			$scope.deleteSelected = function(){
				if(window.confirm('Esta seguro que desea eliminar los pedidos seleccionados?')){
					selectedOrders().forEach(function(order){
						var index = $scope.orders.indexOf(order);
						$scope.orders.splice(index, 1);
					});
					$scope.recalc();
				}
			};
			$scope.logistics = function(){
				var modalInstance = $modal.open({
					templateUrl: 'views/logistics.html',
					controller: 'LogisticsCtrl',
					resolve: {
						orders: function () {
							return selectedOrders();
						}
					},
					windowClass: 'logistics-modal'
				});
				modalInstance.result.then(function(){
					$scope.recalc();
				});
			};
		}]);

angular.module('prototipoApp')
	.controller('LogisticsCtrl', function ($scope,$modalInstance,orders) {
		var height = 200;
		$scope.orders = orders;
		$scope.getHeight = function(){
			return height;
		};
		$scope.beforeRender = function(){
			var MAX_ROWS = 15;
			if(this.countRows()>MAX_ROWS){
				height =  27+23*MAX_ROWS;
			}
			else {
				height = Infinity;
			}
		};
		$scope.deliverynote = undefined;
		$scope.method = undefined;
		$scope.tracking = undefined;
		var multiUpdate = function(property,newValue){
			$scope.orders.forEach(function(order){
				order[property] = newValue;
			});
		};
		var watchProperty = function(property){
			$scope.$watch(property,function(newValue){
				multiUpdate(property,newValue);
			});
		};
		watchProperty('deliverynote');
		watchProperty('method');
		watchProperty('tracking');

		var deleteProperties = function(){
			var properties = ['deliverynote','method','tracking'];
			$scope.orders.forEach(function(order){
				properties.forEach(function(property){
					delete order[property];
				});
			});
		};
		var getShippedQuantityFor = function(order,product){
			var quantity = 0;
			order.shipments.forEach(function(shipment){
				shipment.products.forEach(function(line){
					if(line.product.id===product.id){
						quantity += line.quantity;
					}
				});
			});
			return quantity;
		};
		var addShipment = function(order){
			var calcProducts = order.lines.filter(function(line){ return line.quantity - getShippedQuantityFor(order,line.product) > 0; }).map(function(line){ return {product: line.product, quantity: line.quantity - getShippedQuantityFor(order,line.product)}; });
			order.shipments.push({deliverynote: order.deliverynote, method: order.method, tracking: order.tracking, products: calcProducts});
			order.shippingStatus = 'Done';
			order.selected = false;
		};


		$scope.ok = function () {
			$scope.orders.forEach(addShipment);
			deleteProperties();
			$modalInstance.close();
		};

		$scope.cancel = function () {
			deleteProperties();
			$modalInstance.dismiss('cancel');
		};

	});

