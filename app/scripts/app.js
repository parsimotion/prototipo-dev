'use strict';

angular.module('prototipoApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute',
  'ui.bootstrap',
  'ui.select2',
  'uiHandsontable'
])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/sales2', {
        templateUrl: 'views/sales2.html',
        controller: 'SalesCtrl'
      })
      .when('/sales', {
        templateUrl: 'views/sales.html',
        controller: 'SalesCtrl',
        reloadOnSearch: false
      })
      .when('/sales/new', {
        templateUrl: 'views/order-edit.html',
        controller: 'OrderCtrl'
      })
      .when('/sales/:id', {
        templateUrl: 'views/order.html',
        controller: 'OrderCtrl'
      })
      .when('/sales/:id/edit', {
        templateUrl: 'views/order-edit.html',
        controller: 'OrderCtrl'
      })
      .when('/products', {
        templateUrl: 'views/products.html',
        controller: 'ProductsCtrl'
      })
      .when('/products/new', {
        templateUrl: 'views/product-edit.html',
        controller: 'ProductCtrl'
      })
      .when('/products/:id', {
        templateUrl: 'views/product.html',
        controller: 'ProductCtrl'
      })
      .when('/products/:id/edit', {
        templateUrl: 'views/product-edit.html',
        controller: 'ProductCtrl'
      })
      .when('/contacts', {
        templateUrl: 'views/contacts.html',
        controller: 'ContactsCtrl'
      })
      .when('/contacts/new', {
        templateUrl: 'views/contact-edit.html',
        controller: 'ContactCtrl'
      })
      .when('/contacts/:id', {
        templateUrl: 'views/contact.html',
        controller: 'ContactCtrl'
      })
      .when('/contacts/:id/edit', {
        templateUrl: 'views/contact-edit.html',
        controller: 'ContactCtrl'
      })
      .otherwise({
        redirectTo: '/sales'
      });
  });

angular.module('prototipoApp').directive('xngNavLink', [
  '$location', function($location) {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        return scope.$on('$routeChangeSuccess', function(event, newLocation) {
          var getPathRoot = function(path) {
            return path.split('/')[1];
          };
          if(newLocation.originalPath){
            var currentPathRoot = getPathRoot(newLocation.originalPath);
            var elementPathRoot = getPathRoot($(element).find('a').attr('href'));
            if (elementPathRoot === currentPathRoot) {
              return $(element).addClass('active');
            } else {
              return $(element).removeClass('active');
            }            
          }
        });
      }
    }
  }
]);

angular.module('prototipoApp').directive('xngDatePicker', function() {
  return {
    restrict: 'A',
    scope: {
      xngDatePicker: '='
    },
    link: function(scope, element, attrs) {
      $(element).datepicker({
        format: 'dd/mm/yyyy',
        autoclose: true
      }).on('changeDate', function(selection) {
        return scope.$apply(function() {
          return scope.xngDatePicker = selection.date;
        });
      });
      return scope.$watch('xngDatePicker', function(date) {
        if (date) {
          return $(element).datepicker('update', new Date(date));
        }
      });
    }
  };
});

angular.module('prototipoApp').directive('orders', function() {
  return {
    restrict: 'A',
    scope: {
      orders: '=',
      templateUrl: '='
    },
    template: '<div ng-include="getTemplateUrl()"></div>',
    controller: function($scope) {
      $scope.getTemplateUrl = function(){
        return $scope.templateUrl || 'views/sales-list.html';
      };
      $scope.selectAll = false;
      $scope.toggleSelectAll = function(){
        $scope.selectAll = !$scope.selectAll;
        $scope.orders.forEach(function(order){
          order.selected = $scope.selectAll;
        });
      };
      var watchOrder = function(order){
        order.previousShippingMethod = order.shippingMethod;
        order.previousPaymentMethod = order.paymentMethod;
        var unsubscribe = $scope.$watch(function(){ return order.shippingMethod; },function(newValue,oldValue){
          if(newValue!=oldValue){
            order.editing = true;
            unsubscribe();
          }
        });        
        var unsubscribe2 = $scope.$watch(function(){ return order.paymentMethod; },function(newValue,oldValue){
          if(newValue!=oldValue){
            order.editing = true;
            unsubscribe2();
          }
        });        
      }
      $scope.orders.forEach(watchOrder);
      $scope.save = function(order){
        order.editing = false;
        watchOrder(order);
        $scope.$parent.recalc();  
      }
      $scope.cancel = function(order){
        order.shippingMethod = order.previousShippingMethod;
        order.paymentMethod = order.previousPaymentMethod;
        order.editing = false;
        watchOrder(order);
      }
      $scope.anySelected = function(){
        return $scope.orders.filter(function(order){ return order.selected; }).length > 0;
      };      
    }
  };
});


angular.module('prototipoApp').run(['uiSelect2Config','$filter', function(uiSelect2Config,$filter) {
    uiSelect2Config.placeholder = " ";
    uiSelect2Config.query = function(query) {
        var data = {results: []};
        if(!this.simple_tags){
          $filter('filter')(this.data,query.term).forEach(function(item){data.results.push({id: item, text: item});});
        }
        query.callback(data);          
      };
    uiSelect2Config.createSearchChoice = function(term,data){
      if(this.opts.allowNotListed && term && data.length===0){
        return {id: term, text: term};
      }
    };
    uiSelect2Config.initSelection = function(element, callback) {
      if(element.select2("val")){
        callback(element.select2("val"));
      }
    };
    uiSelect2Config.formatResult = function(value) {
      return value.id;
    };    
}]);