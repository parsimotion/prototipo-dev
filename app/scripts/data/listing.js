window.category = {
    "id": "MLA4154",
    "name": "Zapatos y Sandalias",
    "picture": null,
    "permalink": null,
    "total_items_in_this_category": 2544,
    "path_from_root": [
        {
            "id": "MLA1430",
            "name": "Ropa y Accesorios"
        },
        {
            "id": "MLA109026",
            "name": "Zapatos y Sandalias"
        },
        {
            "id": "MLA3120",
            "name": "Hombre"
        },
        {
            "id": "MLA4154",
            "name": "Zapatos y Sandalias"
        }
    ],
    "children_categories": [],
    "attribute_types": "variations",
    "settings": {
        "adult_content": false,
        "buying_allowed": true,
        "buying_modes": [
            "auction",
            "buy_it_now"
        ],
        "coverage_areas": "not_allowed",
        "currencies": [
            "ARS"
        ],
        "immediate_payment": "optional",
        "item_conditions": [
            "new",
            "used",
            "not_specified"
        ],
        "items_reviews_allowed": false,
        "listing_allowed": true,
        "max_description_length": 50000,
        "max_pictures_per_item": 36,
        "max_sub_title_length": 70,
        "max_title_length": 60,
        "maximum_price": null,
        "minimum_price": null,
        "mirror_category": null,
        "price": "required",
        "restrictions": [],
        "rounded_address": false,
        "seller_contact": "not_allowed",
        "shipping_modes": [
            "not_specified",
            "me2",
            "me1",
            "custom"
        ],
        "shipping_options": [
            "custom",
            "carrier"
        ],
        "shipping_profile": "optional",
        "show_contact_information": false,
        "simple_shipping": "optional",
        "stock": "required",
        "vip_subdomain": "articulo",
        "tags": []
    }
};
window.listing = {
    "id": "MLA487491175",
    "site_id": "MLA",
    "title": "Zapatos Para Hombre R-ejal (varios Modelos)",
    "subtitle": "Calzado Fino De Cuero. De Vestir.",
    "seller_id": 36508691,
    "category_id": "MLA4154",
    "official_store_id": null,
    "price": 330,
    "base_price": 330,
    "currency_id": "ARS",
    "initial_quantity": 23,
    "available_quantity": 9,
    "sold_quantity": 40,
    "buying_mode": "buy_it_now",
    "listing_type_id": "gold",
    "start_time": "2013-12-10T01:11:58.000Z",
    "stop_time": "2014-02-08T01:11:58.000Z",
    "condition": "new",
    "permalink": "http://articulo.mercadolibre.com.ar/MLA-487491175-zapatos-para-hombre-r-ejal-varios-modelos-_JM",
    "thumbnail": "http://mla-s1-p.mlstatic.com/4152-MLA2791612404_062012-I.jpg",
    "secure_thumbnail": "https://a248.e.akamai.net/mla-s1-p.mlstatic.com/4152-MLA2791612404_062012-I.jpg",
    "pictures": [
        {
            "id": "4152-MLA2791612404_062012",
            "url": "http://mla-s1-p.mlstatic.com/4152-MLA2791612404_062012-O.jpg",
            "secure_url": "https://a248.e.akamai.net/mla-s1-p.mlstatic.com/4152-MLA2791612404_062012-O.jpg",
            "size": "500x375",
            "max_size": "720x540",
            "quality": ""
        },
        {
            "id": "4188-MLA2791638383_062012",
            "url": "http://mla-s1-p.mlstatic.com/4188-MLA2791638383_062012-O.jpg",
            "secure_url": "https://a248.e.akamai.net/mla-s1-p.mlstatic.com/4188-MLA2791638383_062012-O.jpg",
            "size": "500x375",
            "max_size": "720x540",
            "quality": ""
        },
        {
            "id": "4117-MLA2791618079_062012",
            "url": "http://mla-s1-p.mlstatic.com/4117-MLA2791618079_062012-O.jpg",
            "secure_url": "https://a248.e.akamai.net/mla-s1-p.mlstatic.com/4117-MLA2791618079_062012-O.jpg",
            "size": "500x375",
            "max_size": "720x540",
            "quality": ""
        },
        {
            "id": "4161-MLA2791618389_062012",
            "url": "http://mla-s1-p.mlstatic.com/4161-MLA2791618389_062012-O.jpg",
            "secure_url": "https://a248.e.akamai.net/mla-s1-p.mlstatic.com/4161-MLA2791618389_062012-O.jpg",
            "size": "500x375",
            "max_size": "720x540",
            "quality": ""
        },
        {
            "id": "4188-MLA2791617721_062012",
            "url": "http://mla-s2-p.mlstatic.com/4188-MLA2791617721_062012-O.jpg",
            "secure_url": "https://a248.e.akamai.net/mla-s2-p.mlstatic.com/4188-MLA2791617721_062012-O.jpg",
            "size": "500x375",
            "max_size": "720x540",
            "quality": ""
        },
        {
            "id": "4126-MLA2791618611_062012",
            "url": "http://mla-s1-p.mlstatic.com/4126-MLA2791618611_062012-O.jpg",
            "secure_url": "https://a248.e.akamai.net/mla-s1-p.mlstatic.com/4126-MLA2791618611_062012-O.jpg",
            "size": "500x375",
            "max_size": "720x540",
            "quality": ""
        },
        {
            "id": "MLA3053062924_082012",
            "url": "http://img1.mlstatic.com/s_MLA_v_O_f_3053062924_082012.jpg",
            "secure_url": "https://www.mercadolibre.com/jm/img?s=MLA&v=O&f=3053062924_082012.jpg",
            "size": "500x375",
            "max_size": "720x540",
            "quality": ""
        },
        {
            "id": "MLA3053087117_082012",
            "url": "http://img2.mlstatic.com/s_MLA_v_O_f_3053087117_082012.jpg",
            "secure_url": "https://www.mercadolibre.com/jm/img?s=MLA&v=O&f=3053087117_082012.jpg",
            "size": "500x375",
            "max_size": "720x540",
            "quality": ""
        },
        {
            "id": "MLA3053079348_082012",
            "url": "http://img1.mlstatic.com/s_MLA_v_O_f_3053079348_082012.jpg",
            "secure_url": "https://www.mercadolibre.com/jm/img?s=MLA&v=O&f=3053079348_082012.jpg",
            "size": "500x375",
            "max_size": "720x540",
            "quality": ""
        }
    ],
    "video_id": null,
    "descriptions": [
        {
            "id": "MLA487491175-505421849"
        }
    ],
    "accepts_mercadopago": true,
    "non_mercado_pago_payment_methods": [
        {
            "id": "MLATB",
            "description": "Transferencia bancaria",
            "type": "G"
        },
        {
            "id": "MLAWC",
            "description": "Acordar con el comprador",
            "type": "G"
        },
        {
            "id": "MLAMO",
            "description": "Efectivo",
            "type": "G"
        }
    ],
    "shipping": {
        "profile_id": null,
        "mode": "not_specified",
        "local_pick_up": false,
        "free_shipping": false,
        "methods": [],
        "dimensions": null
    },
    "seller_address": {
        "id": 64896887,
        "comment": "",
        "address_line": "",
        "zip_code": "",
        "city": {
            "id": "TUxBQ0xBTmJjYzE4",
            "name": "Lanús"
        },
        "state": {
            "id": "AR-B",
            "name": "Buenos Aires"
        },
        "country": {
            "id": "AR",
            "name": "Argentina"
        },
        "latitude": "",
        "longitude": "",
        "search_location": {
            "neighborhood": {
                "id": "",
                "name": ""
            },
            "city": {
                "id": "",
                "name": ""
            },
            "state": {
                "id": "TUxBUFpPTmFpbnRl",
                "name": "Buenos Aires Interior"
            }
        }
    },
    "seller_contact": null,
    "location": null,
    "geolocation": {
        "latitude": "",
        "longitude": ""
    },
    "coverage_areas": [],
    "attributes": [],
    "listing_source": "",
    "variations": [
        {
            "id": 5522688880,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73001",
                    "name": "Color Secundario",
                    "value_id": "82058",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82069",
                    "value_name": "39"
                }
            ],
            "price": 330,
            "available_quantity": 1,
            "sold_quantity": 3,
            "picture_ids": [
                "4152-MLA2791612404_062012",
                "4188-MLA2791638383_062012",
                "4117-MLA2791618079_062012",
                "4161-MLA2791618389_062012",
                "4188-MLA2791617721_062012",
                "4126-MLA2791618611_062012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688884,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73001",
                    "name": "Color Secundario",
                    "value_id": "82058",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82079",
                    "value_name": "44"
                }
            ],
            "price": 330,
            "available_quantity": 0,
            "sold_quantity": 4,
            "picture_ids": [
                "4152-MLA2791612404_062012",
                "4188-MLA2791638383_062012",
                "4117-MLA2791618079_062012",
                "4161-MLA2791618389_062012",
                "4188-MLA2791617721_062012",
                "4126-MLA2791618611_062012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688888,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73001",
                    "name": "Color Secundario",
                    "value_id": "82058",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82073",
                    "value_name": "41"
                }
            ],
            "price": 330,
            "available_quantity": 0,
            "sold_quantity": 5,
            "picture_ids": [
                "4152-MLA2791612404_062012",
                "4188-MLA2791638383_062012",
                "4117-MLA2791618079_062012",
                "4161-MLA2791618389_062012",
                "4188-MLA2791617721_062012",
                "4126-MLA2791618611_062012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688892,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73001",
                    "name": "Color Secundario",
                    "value_id": "82058",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82081",
                    "value_name": "45"
                }
            ],
            "price": 330,
            "available_quantity": 0,
            "sold_quantity": 2,
            "picture_ids": [
                "4152-MLA2791612404_062012",
                "4188-MLA2791638383_062012",
                "4117-MLA2791618079_062012",
                "4161-MLA2791618389_062012",
                "4188-MLA2791617721_062012",
                "4126-MLA2791618611_062012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688896,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73001",
                    "name": "Color Secundario",
                    "value_id": "82058",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82083",
                    "value_name": "46"
                }
            ],
            "price": 330,
            "available_quantity": 0,
            "sold_quantity": 2,
            "picture_ids": [
                "4152-MLA2791612404_062012",
                "4188-MLA2791638383_062012",
                "4117-MLA2791618079_062012",
                "4161-MLA2791618389_062012",
                "4188-MLA2791617721_062012",
                "4126-MLA2791618611_062012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688900,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73001",
                    "name": "Color Secundario",
                    "value_id": "82058",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82071",
                    "value_name": "40"
                }
            ],
            "price": 330,
            "available_quantity": 0,
            "sold_quantity": 4,
            "picture_ids": [
                "4152-MLA2791612404_062012",
                "4188-MLA2791638383_062012",
                "4117-MLA2791618079_062012",
                "4161-MLA2791618389_062012",
                "4188-MLA2791617721_062012",
                "4126-MLA2791618611_062012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688904,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82071",
                    "value_name": "40"
                }
            ],
            "price": 330,
            "available_quantity": 1,
            "sold_quantity": 1,
            "picture_ids": [
                "MLA3053062924_082012",
                "MLA3053087117_082012",
                "MLA3053079348_082012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688907,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82068",
                    "value_name": "38"
                }
            ],
            "price": 330,
            "available_quantity": 1,
            "sold_quantity": 1,
            "picture_ids": [
                "MLA3053062924_082012",
                "MLA3053087117_082012",
                "MLA3053079348_082012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688910,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82069",
                    "value_name": "39"
                }
            ],
            "price": 330,
            "available_quantity": 1,
            "sold_quantity": 1,
            "picture_ids": [
                "MLA3053062924_082012",
                "MLA3053087117_082012",
                "MLA3053079348_082012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688913,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82075",
                    "value_name": "42"
                }
            ],
            "price": 330,
            "available_quantity": 0,
            "sold_quantity": 2,
            "picture_ids": [
                "MLA3053062924_082012",
                "MLA3053087117_082012",
                "MLA3053079348_082012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688916,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82077",
                    "value_name": "43"
                }
            ],
            "price": 330,
            "available_quantity": 0,
            "sold_quantity": 2,
            "picture_ids": [
                "MLA3053062924_082012",
                "MLA3053087117_082012",
                "MLA3053079348_082012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688919,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82079",
                    "value_name": "44"
                }
            ],
            "price": 330,
            "available_quantity": 0,
            "sold_quantity": 2,
            "picture_ids": [
                "MLA3053062924_082012",
                "MLA3053087117_082012",
                "MLA3053079348_082012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688922,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82083",
                    "value_name": "46"
                }
            ],
            "price": 330,
            "available_quantity": 1,
            "sold_quantity": 1,
            "picture_ids": [
                "MLA3053062924_082012",
                "MLA3053087117_082012",
                "MLA3053079348_082012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688925,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82081",
                    "value_name": "45"
                }
            ],
            "price": 330,
            "available_quantity": 0,
            "sold_quantity": 2,
            "picture_ids": [
                "MLA3053062924_082012",
                "MLA3053087117_082012",
                "MLA3053079348_082012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688928,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82073",
                    "value_name": "41"
                }
            ],
            "price": 330,
            "available_quantity": 1,
            "sold_quantity": 1,
            "picture_ids": [
                "MLA3053062924_082012",
                "MLA3053087117_082012",
                "MLA3053079348_082012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688931,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73001",
                    "name": "Color Secundario",
                    "value_id": "82058",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82077",
                    "value_name": "43"
                }
            ],
            "price": 330,
            "available_quantity": 1,
            "sold_quantity": 3,
            "picture_ids": [
                "4152-MLA2791612404_062012",
                "4188-MLA2791638383_062012",
                "4117-MLA2791618079_062012",
                "4161-MLA2791618389_062012",
                "4188-MLA2791617721_062012",
                "4126-MLA2791618611_062012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688935,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73001",
                    "name": "Color Secundario",
                    "value_id": "82058",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82075",
                    "value_name": "42"
                }
            ],
            "price": 330,
            "available_quantity": 1,
            "sold_quantity": 3,
            "picture_ids": [
                "4152-MLA2791612404_062012",
                "4188-MLA2791638383_062012",
                "4117-MLA2791618079_062012",
                "4161-MLA2791618389_062012",
                "4188-MLA2791617721_062012",
                "4126-MLA2791618611_062012"
            ],
            "seller_custom_field": null
        },
        {
            "id": 5522688939,
            "attribute_combinations": [
                {
                    "id": "83000",
                    "name": "Color Primario",
                    "value_id": "92025",
                    "value_name": "Negro"
                },
                {
                    "id": "73001",
                    "name": "Color Secundario",
                    "value_id": "82058",
                    "value_name": "Negro"
                },
                {
                    "id": "73002",
                    "name": "Talle",
                    "value_id": "82068",
                    "value_name": "38"
                }
            ],
            "price": 330,
            "available_quantity": 1,
            "sold_quantity": 1,
            "picture_ids": [
                "4152-MLA2791612404_062012",
                "4188-MLA2791638383_062012",
                "4117-MLA2791618079_062012",
                "4161-MLA2791618389_062012",
                "4188-MLA2791617721_062012",
                "4126-MLA2791618611_062012"
            ],
            "seller_custom_field": null
        }
    ],
    "status": "paused",
    "sub_status": [],
    "tags": [
        "dragged_bids_and_visits"
    ],
    "warranty": null,
    "catalog_product_id": null,
    "parent_item_id": "MLA480793338",
    "automatic_relist": false,
    "date_created": "2013-12-10T01:11:58.000Z",
    "last_updated": "2014-01-17T18:04:11.000Z"
}